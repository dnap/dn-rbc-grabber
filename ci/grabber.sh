#!/bin/bash
set -ex

sleep 5 # wait load db

while true; do
  php bin/console app:grab
  sleep 600
done
