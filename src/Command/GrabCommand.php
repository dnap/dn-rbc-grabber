<?php
namespace App\Command;

use App\Repository\ArticleRepository;
use App\Services\Enrichments\EnrichmentServiceInterface;
use App\Services\GrabberInterface;
use GuzzleHttp\Psr7\Uri;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\ConsoleOutputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;

class GrabCommand extends Command
{
    protected static $defaultName = 'app:grab';

    private GrabberInterface $grabber;
    private EnrichmentServiceInterface $enrichmentService;
    private ArticleRepository $articleRepository;

    public function __construct(
        GrabberInterface $grabber,
        EnrichmentServiceInterface $enrichmentService,
        ArticleRepository $articleRepository
    ) {
        parent::__construct();
        $this->grabber = $grabber;
        $this->enrichmentService = $enrichmentService;
        $this->articleRepository = $articleRepository;
    }

    protected function configure(): void
    {
        $this->setDescription('Grab rbc');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $stdErr = $output;
        if ($output instanceof ConsoleOutputInterface) {
            $stdErr = $output->getErrorOutput();
        }

        $articles = $this->grabber->grab(new Uri('https://www.rbc.ru/'));
        foreach ($articles as $article) {
            try {
                if ($this->articleRepository->exists($article->getId())) {
                    continue;
                }
                $this->enrichmentService->enrichment($article);
                $this->articleRepository->insert($article);
            } catch (Throwable $e) {
                $stdErr->writeln($e);
            }
        }

        return Command::SUCCESS;
    }
}