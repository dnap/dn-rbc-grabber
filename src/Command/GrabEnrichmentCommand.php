<?php

namespace App\Command;

use App\Entity\Article;
use App\Services\Enrichments\EnrichmentServiceInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GrabEnrichmentCommand extends Command
{
    protected static $defaultName = 'debug:grab:enrichment';

    private EnrichmentServiceInterface $enrichmentService;

    public function __construct(EnrichmentServiceInterface $enrichmentService)
    {
        parent::__construct();
        $this->enrichmentService = $enrichmentService;
    }

    protected function configure(): void
    {
        $this->addArgument('url', InputArgument::REQUIRED, 'Url for enrichment');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $article = new Article();
        $article->setUrl($input->getArgument('url'));
        $article->setId('test');

        $this->enrichmentService->enrichment($article);

        $output->writeln('title: '.$article->getTitle());
        $output->writeln('description: '.$article->getDescription());
        $output->writeln('imageUrl: '.$article->getImageUrl());

        return Command::SUCCESS;
    }
}