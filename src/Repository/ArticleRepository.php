<?php

namespace App\Repository;

use App\Entity\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Article::class);
    }

    public function exists(string $id): bool
    {
        $query = $this->createQueryBuilder('a')
            ->select('a.id')
            ->andWhere('a.id = :id')
            ->setParameter('id', $id)
            ->getQuery();

        return (bool)$query->getOneOrNullResult(Query::HYDRATE_SINGLE_SCALAR);
    }

    public function findLatest($limit = 20): array
    {
        return $this->findBy([], ['createdAt' => 'DESC'], $limit);
    }

    public function insert(Article $article): void
    {
        $entityManager = $this->getEntityManager();
        $entityManager->persist($article);
        $entityManager->flush();
    }
}
