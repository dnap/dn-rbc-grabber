<?php

namespace App\Services\Enrichments;

use App\Entity\Article;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\TransferStats;
use PHPHtmlParser\Contracts\DomInterface;
use PHPHtmlParser\Dom;

abstract class AbstractDomEnrichmentService implements EnrichmentServiceInterface
{
    protected DomInterface $dom;
    private ClientInterface $client;

    public function __construct()
    {
        $this->dom = new Dom();
        $this->client = new Client();
    }

    protected function loadContent(Article $article): void
    {
        $response = $this->client->request(
            'GET',
            $article->getUrl(),
            [
                RequestOptions::ALLOW_REDIRECTS => true,
                RequestOptions::ON_STATS => function (TransferStats $stats) use ($article) {
                    // update after redirect
                    $article->setUrl((string)$stats->getEffectiveUri());
                },
            ]
        );
        $this->dom->loadStr($response->getBody()->getContents());
    }
}