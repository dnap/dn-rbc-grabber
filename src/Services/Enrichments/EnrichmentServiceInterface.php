<?php

namespace App\Services\Enrichments;

use App\Entity\Article;

interface EnrichmentServiceInterface
{
    public function enrichment(Article $article): void;
}