<?php

namespace App\Services\Enrichments;

use App\Entity\Article;
use App\Exceptions\NotFoundContentException;
use GuzzleHttp\Psr7\Uri;
use Psr\Log\LoggerInterface;
use RuntimeException;

class EnrichmentStrategy implements EnrichmentServiceInterface
{
    /**
     * Partial contains www.rbc.ru or sportrbc.ru
     */
    private const RBC_HOST_REGEXP = '/rbc\.ru$/';

    private RBCEnrichmentService $rbc;
    private OpenGraphEnrichmentService $openGraph;
    private LoggerInterface $logger;

    public function __construct(
        RBCEnrichmentService $rbc,
        OpenGraphEnrichmentService $openGraph,
        LoggerInterface $logger
    ) {
        $this->rbc = $rbc;
        $this->openGraph = $openGraph;
        $this->logger = $logger;
    }

    public function enrichment(Article $article): void
    {
        $url = $article->getUrl();
        if ($url === null) {
            throw new RuntimeException('Cannot create enrichment service without url.');
        }
        $uri = new Uri($url);
        // todo traffic.rbc.ru
        if (preg_match(self::RBC_HOST_REGEXP, $uri->getHost()) === 1) {
            try {
                $this->rbc->enrichment($article);

                return;
            } catch (NotFoundContentException $e) {
                $this->logger->error(
                    'Rbc Enrichment not found content for url: ' . $article->getUrl(),
                    ['exception' => $e]
                );
            }
        }


        $this->openGraph->enrichment($article);
    }
}
