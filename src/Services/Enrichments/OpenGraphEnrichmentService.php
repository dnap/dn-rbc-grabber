<?php

namespace App\Services\Enrichments;

use App\Entity\Article;
use GuzzleHttp\Psr7\Uri;
use PHPHtmlParser\Dom\Node\HtmlNode;

class OpenGraphEnrichmentService extends AbstractDomEnrichmentService
{
    public function enrichment(Article $article): void
    {
        $this->loadContent($article);
        $metaTags = $this->dom->find('head meta');

        /** @var HtmlNode $metaTag */
        foreach ($metaTags as $metaTag) {
            $this->parseTag($metaTag, $article);
        }
    }

    private function parseTag(HtmlNode $metaTag, Article $article): void
    {
        $attribute = $metaTag->getAttribute('property');
        $content = $metaTag->getAttribute('content');
        if ($attribute === null || $content === null) {
            return;
        }
        $content = html_entity_decode($content);
        switch (strtolower($attribute)) {
            case 'og:title':
                $article->setTitle($content);
                break;
            case 'og:url':
                $article->setUrl($content);
                break;
            case 'og:image':
                if ($content[0] === '/') {
                    $uri = new Uri($article->getUrl());
                    $content = Uri::composeComponents($uri->getScheme(), $uri->getAuthority(), $content, '', '');
                }
                $article->setImageUrl($content);
                break;
        }
    }
}