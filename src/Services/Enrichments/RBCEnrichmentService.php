<?php

namespace App\Services\Enrichments;

use App\Entity\Article;
use App\Exceptions\NotFoundContentException;
use PHPHtmlParser\Contracts\DomInterface;
use PHPHtmlParser\Dom\Node\HtmlNode;

class RBCEnrichmentService extends AbstractDomEnrichmentService
{
    public function enrichment(Article $article): void
    {
        $this->loadContent($article);

        /** @var ?HtmlNode $articleNode */
        $articleNode = $this->dom->find('.article__text', 0);
        if ($articleNode === null) {
            throw new NotFoundContentException(
                'RBC content not found in article #' . $article->getId() . "\nUrl: " . $article->getUrl()
            );
        }
        $this->extractTitle($this->dom, $article);
        $this->extractImage($articleNode, $article);
        $this->extractDescription($articleNode, $article);
    }

    private function extractImage(HtmlNode $articleNode, Article $article): void
    {
        /** @var ?HtmlNode $imageNode */
        $imageNode = $articleNode->find('img.article__main-image__image', 0);
        if ($imageNode !== null) {
            $article->setImageUrl($imageNode->getAttribute('src'));
        }
    }

    private function extractTitle(DomInterface $dom, Article $article): void
    {
        /** @var ?HtmlNode $imageNode */
        $textNode = $dom->find('.article__header__title h1', 0);
        if ($textNode !== null) {
            $article->setTitle(html_entity_decode($textNode->innerText()));
        }
    }

    private function extractDescription(HtmlNode $articleNode, Article $article): void
    {
        /** @var ?HtmlNode $imageNode */
        $textNode = $articleNode->find('article__text__overview', 0);
        if ($textNode !== null) {
            $article->setDescription(html_entity_decode($textNode->innerText()));

            return;
        }
        $textNode = $articleNode->find('p', 0);
        if ($textNode !== null) {
            $article->setDescription(html_entity_decode($textNode->innerText()));
        }
    }
}