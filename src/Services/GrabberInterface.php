<?php
namespace App\Services;

use App\Entity\Article;
use Psr\Http\Message\UriInterface;

interface GrabberInterface
{
    /**
     * @param UriInterface $uri
     *
     * @return Article[]
     */
    public function grab(UriInterface $uri): array;
}