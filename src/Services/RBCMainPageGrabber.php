<?php

namespace App\Services;

use App\Entity\Article;
use App\Exceptions\GrabException;
use PHPHtmlParser\Contracts\DomInterface;
use PHPHtmlParser\Dom;
use PHPHtmlParser\Dom\Node\HtmlNode;
use Psr\Http\Message\UriInterface;

class RBCMainPageGrabber implements GrabberInterface
{
    private DomInterface $dom;

    public function __construct(DomInterface $dom = null)
    {
        if ($dom === null) {
            $this->dom = new Dom();
        } else {
            $this->dom = $dom;
        }
    }

    public function grab(UriInterface $uri): array
    {
        $this->dom->loadFromUrl($uri);
        $nodeCollection = $this->dom->find('.js-news-feed-list a.news-feed__item');
        $pages = [];
        /** @var HtmlNode $node */
        foreach ($nodeCollection as $node) {
            try {
                $page = $this->createArticle($node);
                if ($page !== null) {
                    $pages[] = $page;
                }
            } catch (GrabException $e) {
                // todo add logging
            }
        }

        return $pages;
    }

    private function createArticle(HtmlNode $node): Article
    {
        $id = $this->extractId($node->getAttribute('id'));
        $url = $node->getAttribute('href');
        if ($url === null) {
            throw new GrabException('Empty href attribute');
        }

        $article = new Article();
        $article
            ->setId($id)
            ->setUrl(html_entity_decode($url))
            ->setTitle($this->extractTitle($node));

        return $article;
    }

    private function extractId(?string $attributeId): string
    {
        if ($attributeId === null) {
            throw new GrabException('Empty id attribute');
        }
        $idPats = explode('_', $attributeId);
        $id = end($idPats);
        if (strlen($id) !== 24) {
            throw new GrabException('Invalid id: ' . $attributeId);
        }

        return $id;
    }

    private function extractTitle(HtmlNode $node): ?string
    {
        /** @var HtmlNode|false $node */
        $subNode = $node->find('.news-feed__item__title', 0);
        if ($subNode === null) {
            throw new GrabException('Invalid title');
        }

        return html_entity_decode($subNode->innerText());
    }
}